<?php 
define( 'WP_USE_THEMES', false );
require( '/home/lgonzales/workspace/inkafarma_web/wp-load.php' );
class demo {
static $options;
static function prueba() { 
self::$options = get_option( 'jycform' );
$name = $_POST['name'];
$lastname = $_POST['lastname'];
$email = $_POST['email'];
$type_doc = $_POST['type_doc'];
$number_doc = $_POST['number_doc'];
$name_rep = $_POST['name_rep'];
$lastname_rep = $_POST['lastname_rep'];
$type_doc_rep = $_POST['type_doc_rep'];
$number_doc_rep = $_POST['number_doc_rep'];
$tipo_solicitud = $_POST['tipo_solicitud'];
$address = $_POST['address'];
$descrip_exponga = $_POST['descrip_exponga'];
$last_id = str_pad(1, 6, 0, STR_PAD_LEFT);


$tipo_a = ($tipo_solicitud == 'ACCESO') ? 'X' : ' ';
$tipo_r = ($tipo_solicitud == 'RECTIFICACION') ? 'X' : ' ';
$tipo_c = ($tipo_solicitud == 'CANCELACION') ? 'X' : ' ';
$tipo_o = ($tipo_solicitud == 'OPOSICION') ? 'X' : ' ';
$contenido = "
<!DOCTYPE html>
<html>
  <head>
    <style>
    html,body{
	    height:297mm;
	    width:210mm;
	}
	body {
        /*height: 842px;
        width: 595px;*/
        margin-left: auto;
        margin-right: auto;
    }
    </style>
  </head>
  <body>
	<table width='100%' border='0'>
		<tr>
			<td width='150px' align='center'>" . self::$options['generaltitle'] . "</td>
			<td></td>
			<td style='padding-right:20px' align='right'><img src='" . get_site_url() . "/wp-content/plugins/jyc-form/includes/theme/asset/img/logo.png' /></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td style='padding-right:100px' align='right'> " . date('d/m/Y H:i:s') . "</td>
		</tr>
		<tr>
			<td align='center'>" . self::$options['generalsubtitle'] . "</td>
			<td></td>
			<td style='padding-right:100px' align='right'> Solicitud Nro " . $last_id . "</td>
		</tr>
		<tr>
			<td height='50px'></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><strong>" . self::$options['generalsubtitle'] . "</strong></p></td>
		</tr>
		<tr>
			<td height='10px'></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td colspan='3' align='left' bgcolor='#CCC'><p style='margin-left:5px;'><strong>DATOS DEL TITULAR DE LOS    DATOS PERSONALES</strong></p></td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Nombres</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $name . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Apellidos</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'> " . $lastname . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Tipo Documento</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $type_doc . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Nro de Documento</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $number_doc . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Email</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $email . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Domicilio</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $address . "</td>
		</tr>
		<tr>
			<td colspan='3' align='left' bgcolor='#CCC'><p style='margin-left:5px;'><strong>DATOS DEL REPRESENTANTE DEL TITULAR DE LOS DATOS PERSONALES (de ser el caso)</strong></p></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><span style='margin-left:5px;'>El presente formulario deberá ser contestado únicamente por el representante legal del titular</span></td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Nombres</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $name_rep . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Apellidos</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $lastname_rep . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Tipo Documento</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $type_doc_rep . "</td>
		</tr>
		<tr>
			<td align='right'><p style='margin-left:5px;'><strong>Nro de Documento</strong></p></td>
			<td width='10px' align='left'><strong style='margin-left:2px;'>:</strong></td>
			<td align='left'>" . $number_doc_rep . "</td>
		</tr>
		<tr>
			<td colspan='3' align='left'><span style='margin-left:5px;'><strong>Seleccione el tipo de solicitud que desea ingresar:</strong></span></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><span style='margin-left:5px;'>Marque con un aspa \"X\" en el recuadro correspondiente al tipo de solicitud de desea ingresar</span></td>
		</tr>
		<tr>
			<td colspan='3'>
				<table width='100%'>
					<tr>
						<td width='150px' align='left'><p style='margin-left:15px;'><strong>ACCESO</strong></p></td>
						<td align='left' style='padding-left:20px;'>" . self::$options['ginfoacceso'] . "</td>
						<td width='20px' align='right' style='padding-right:20px;'>" . $tipo_a . "</td>
					</tr>
					<tr>
						<td align='left'><p style='margin-left:15px;'><strong>RECTIFICACIÓN</strong></p></td>
						<td align='left' style='padding-left:20px;'>" . self::$options['ginforectificacion'] . "</td>
						<td align='right' style='padding-right:20px;'>" . $tipo_r . "</td>
					</tr>
					<tr>
						<td align='left'><p style='margin-left:15px;'><strong>CANCELACIÓN</strong></p></td>
						<td align='left' style='padding-left:20px;'>" . self::$options['ginfocancelacion'] . "</td>
						<td align='right' style='padding-right:20px;'>" . $tipo_c . "</td>
					</tr>
					<tr>
						<td align='left'><p style='margin-left:15px;'><strong>OPOSICIÓN</strong></p></td>
						<td align='left' style='padding-left:20px;'>" . self::$options['ginfooposicion'] . "</td>
						<td align='right' style='padding-right:20px;'>" . $tipo_o . "</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='3' align='left'><span style='margin-left:5px;'><strong>Exponga</strong> de forma clara y precisa en el espacio en blanco los alcances de su solicitud, señalando la información a la que desea acceder, rectificar, cancelar o respecto de la cual desea formular oposición:</span></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><p style='margin-left:5px;'>" . nl2br($descrip_exponga) . "</p></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><span style='margin-left:5px;'>Si cuenta con información adicional y/o documentos que faciliten la localización de los datos personales relacionados con la presente solicitud, por favor adjuntar copia de estos.</span></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><p style='margin-left:5px;'><strong>NOTIFICACIÓN DE LA RESPUESTA</strong></p></td>
		</tr>
		<tr>
			<td colspan='3' align='left'><p style='margin-left:5px;'>" . self::$options['onotification'] . "</p></td>
		</tr>
	</table>
  </body>
</html>

";
echo $contenido;
}
}
demo::prueba();